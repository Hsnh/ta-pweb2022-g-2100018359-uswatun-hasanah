<style>
	body{
		text-align: center;
		background: linear-gradient(to bottom left, #ff66ff 0%, #ff99cc 100%);
		margin: 350px;
		font-family: sans-serif;
		font-size: 24px;
	}
</style>
<body>
	<?php
		$arrBuah = array("Mangga", "Apel", "Pisang", " Kedondong", "Jeruk");
		if(in_array("Jeruk", $arrBuah)){
			echo "Ada buah Jeruk di dalam Array tersebut!";
		}else{
			echo "Tidak ada buah Jeruk di dalam Array tersebut!";
		}
	?>
</body>
<style>
	body{
		text-align: center;
		background: linear-gradient(to bottom right, #993366 0%, #cc6699 100%);
		margin: 32px;
		font-family: sans-serif;
		font-size: 22px;
	}
</style>
<br><br><br>
<body>
	<?php 
		//fungsi tanpa return value, & tanpa parameter
	 	function cetak_ganjil(){
	 		for ($i=0; $i<100; $i++) { 
	 			if ($i%2==1) {
	 				echo "$i, ";
	 			}
	 		}
	 	}
	 	//pemanggilan fungsi
	 	cetak_ganjil();
	 ?>
</body>